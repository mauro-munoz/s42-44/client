// target elements
const loginForm = document.getElementById('login')

loginForm.addEventListener('submit',(e)=>{
    e.preventDefault();

    const email= document.getElementById(`email`).value
    const password= document.getElementById(`password`).value


    fetch(`http://localhost:3000/users/login`,{
        method: 'POST',
        headers: {
            "Content-Type":"application/json"
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    }).then((result)=>{
        let response = result.json();
       response.then((result)=>{
           //store token in the local storage: syntax (nameofkey, value)
           localStorage.setItem('token', result.message )
            //request for user profile and store admin status, and id in local localStorage
            fetch(`http://localhost:3000/users/profile`,{
                method: 'GET',
                headers:{
                    "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            }).then(result=>result.json()).then(result=>{
                // console.log(result),
                //store isAdmin status and _id in local storage to go to profile
            localStorage.setItem(`id`,result._id),
            localStorage.setItem(`admin`,result.isAdmin),
            localStorage.setItem(`fullName`,result.fullName)
            // console.log(typeof result)
            window.location.replace("./products.html")
            alert(`You will be redirected`)}
            )
           //if user is going to logout, localStorage.clear
          
       })
     })
})