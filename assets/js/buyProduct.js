const productId = new URLSearchParams(document.location.search).get("productId")

const productName = document.getElementById('productName')
const description = document.getElementById('description')
const price = document.getElementById('price')
const productContainer = document.getElementById('productContainer')

let userToken = localStorage.getItem('token')

fetch(`http://localhost:3000/products/${productId}`, {
	method: "GET",
	headers: {
		"Authorization": `Bearer ${userToken}`
	}
})
.then(result => result.json())
.then(result => {
	// console.log(result)

	productName.innerHTML = result.productName
	description.innerHTML = result.description
	price.innerHTML = result.price

	productContainer.innerHTML =
	`   
        <div class="form-group">
            <label for="quantity">Quantity:</label>
            <input type="number" id="quantity" required class="form-control">
        </div>
		<button class="btn btn-outline-info btn-block" id="buyButton">
			Buy
		</button>
        
	`


	const buyButton = document.getElementById('buyButton')
    const quantity = document.getElementById('quantity')
	const pName = document.getElementById(`productName`).innerHTML
	// console.log(pName)
	buyButton.addEventListener("click", (e) => {
		e.preventDefault()

		fetch(`http://localhost:3000/orders/add-order`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${userToken}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: pName,
                quantity: quantity.value
			})
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)

			if(result){
				alert('Thank you for your purchase.')
				window.location.replace('./products.html')
			} else {
				alert('Something went wrong.')
			}
		})
	})
})

