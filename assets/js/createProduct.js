// target elements
const token = localStorage.getItem("token")
const createProductForm = document.getElementById('createProductForm')

createProductForm.addEventListener('submit',(e)=>{
    e.preventDefault();

    const productName= document.getElementById(`productName`).value
    const description= document.getElementById(`description`).value
    const price= document.getElementById(`price`).value
    const photo= document.getElementById(`photo`).value


    fetch(`http://localhost:3000/products/add-product`,{
        method: 'POST',
        headers: {
            "Content-Type":"application/json",
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({
            productName: productName,
            description: description,
            price: price,
            pictureName: photo
        })
    }).then(result => result.json().then(result =>{
        alert(result.message)
        alert(`You will be redirected back to Products`)
        window.location.replace("./products.html")
      })).catch(err=>alert(err.message))
      
        
})