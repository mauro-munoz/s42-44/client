
const params = new URLSearchParams(window.location.search)
const specificProductId = params.get("productId")
// target elements
const token = localStorage.getItem("token")



const productName= document.getElementById(`productName`)
const description= document.getElementById(`description`)
const price= document.getElementById(`price`)
//fetch current product 

fetch(`http://localhost:3000/products/${specificProductId}`,{
    method: 'GET',
    headers:{
        "Authorization": `Bearer ${token}`
    }
})
.then (result => result.json())
.then(result => {
    // console.log(result)

    productName.value = result.productName,
    description.value = result.description,
    price.value = result.price


})

const createProductForm = document.getElementById('editProductForm')
//update product
createProductForm.addEventListener('submit',(e)=>{
    e.preventDefault();

    
// console.log(productName, description, price)
//user URL Search Params
    
    // console.log(specificProductId)
    fetch(`http://localhost:3000/products/product-update/${specificProductId}`,{
        method: 'PUT',
        headers: {
            "Content-Type":"application/json",
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({
            productName: productName.value,
            description: description.value,
            price: price.value
        })
    })
    .then(result=>{
        let resp = result.json();
        resp.then(result=>console.log(result))
        alert(`Product ${productName.value} was updated`)
        window.location.replace("./products.html")
    })
})