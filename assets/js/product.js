//get item from local storage

const token = localStorage.getItem("token")
const admin = localStorage.getItem("admin")
const fullName = localStorage.getItem("fullName")
const welcome = document.getElementById("welcome")
welcome.innerHTML=`Welcome ${fullName}`
// console.log(admin)
const productContainer = document.getElementById("anyContainer")
const adminButton = document.getElementById("adminButton")
let cardFooter;
let products;

if(admin == `false`){
    //regular user
    //send request to get all products
        fetch(`http://localhost:3000/products/active-products`, {
            method:"GET",
            headers:{
                "Authorization": `Bearer ${token}`
            }
        })
        //wait for servers response
        .then(result => result.json())
        .then(result => {
            // console.log(result)
        //display the products using card if result array is not empty use length for if condition
            if(result.length < 1){
                return alert(`No Products available`)
            }else{
                products = result.map(product =>{
                   // console.log(product)
                    //destructure
                    let adminButtons = `<a class="btn btn-info m-3" href="./orders.html">Orders</a>`
                    adminButton.innerHTML = adminButtons
                    const {productName, description, price,_id,pictureName} = product
                    return( 
                     `   <div class="col-12 col-md-4">
                            <div class="card" style="width: 18rem;">
								<img src="./../../images/${pictureName}" 
                                class="card-img-top" alt="">
								<div class="card-body">
								  <h5 class="card-title">${productName}</h5>
								  <p class="card-text text-left">${description}</p>
                                  <p class="card-text text-right">${price} php</p>
								  <a href="./buyProduct.html?productId=${_id}" class="btn btn-info">Select Product</a>
								</div>
							  </div>
                        </div>
                        `
                        
                    )
                }).join(" ") 
                // console.log(products)
                productContainer.innerHTML = products
            }
        })

        


} else {
    //admin
    // console.log(`admin`)
     //admin user
    //send request to get all products
    fetch(`http://localhost:3000/products/`, {
        method:"GET",
        headers:{
            "Authorization": `Bearer ${token}`
        }
    })
    //wait for servers response
    .then(result => result.json())
    .then(result => {
        // console.log(result)
    //display the products using card if result array is not empty use length for if condition
        if(result.length < 1){
            let adminButtons =
            `<a class="btn btn-info m-3" href="./createProduct.html">Add Product</a>
            <a class="btn btn-info m-3" href="./dashboard.html">Dashboard</a>
            <a class="btn btn-info m-3" href="./orders.html">Orders</a>`
            adminButton.innerHTML = adminButtons
            return `No Products available`
        }else{
            //return all products by making a copy of the objects in the array
            
            products = result.map(product =>{
            //    console.log(product)
                //destructure
                const {productName, description, price, isOffered, _id,pictureName} = product
            
                if(isOffered){
                    //if course is offered
                    cardFooter = `
                    <a class="btn btn-danger  btn-block" id="" >Delete Product</a>

                    <a class="btn btn-info  btn-block" href="./editProduct.html?productId=${_id}">Edit Product</a>

                    <button type="button" class="btn btn-warning btn-block my-2" data-toggle="modal" data-target="#exampleModal">
                    Archive Product
                  </button>
                  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Archive Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Are you sure you want to archive this course?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="delete-confirmation">Save changes</button>
                        </div>
                        </div>
                    </div>
                    </div> `
                        }
                 else {
                    //if course is not offered
                    cardFooter = `<a class="btn btn-success  btn-block" href="./unarchiveProduct.html">Unarchive</a>
                    <a class="btn btn-danger  btn-block" href="./deleteProduct.html?productId=${_id}">Delete Product</a>`
                }
                 
                return( 
                 `   <div class="col-12 col-md-4">
                        <div class="card my-4 text-center" style="width: 18rem;">
                            <img src="./../../images/${pictureName}" 
                            class="card-img-top" alt="">
                            <div class="card-body">
                              <h5 class="card-title">${productName}</h5>
                              <p class="card-text text-left">${description}</p>
                              <p class="card-text text-right">${price} php</p>
                              <div class="card-footer">
                                    ${cardFooter}
                              </div>
                            </div>
                          </div>
                    </div>
                    `
                )
            }).join(" ") 
            // console.log(products)
            productContainer.innerHTML = products

            let adminButtons = `<a class="btn btn-info m-3" href="./createProduct.html">Add Product</a>
                                 <a class="btn btn-info m-3" href="./dashboard.html">Dashboard</a>
                                 <a class="btn btn-info m-3" href="./orders.html">Orders</a>`
            adminButton.innerHTML = adminButtons

            // const archive = document.getElementById("confirmation")
            // archive.addEventListener(`click`,(e)=>{
            //     e.preventDefault();
            //     //delete product
            //     fetch(`http://localhost:3000/orders/delete-order/${_id}`,{
            //             method: 'DELETE',
            //             headers: {
            //             },
            //             body: JSON.stringify({
            //             })
            //         })
            //         .then(result=>{
            //             let resp = result.json();
            //             resp.then(result=>console.log(result))
            //             alert(`Product ${productName} was updated`)
            //             window.location.replace("./products.html")
            //         })
            
            // })
        }
    })
}