
//target elements
const registerForm = document.getElementById(`register`)

//addEventListener ("event",()=>{})
registerForm.addEventListener("submit",(e)=>{
    //preventDefault will stop reloading/refreshing the screen
    e.preventDefault();
    
    //get the values of the input fields
    const fname= document.getElementById(`name`).value
    const email= document.getElementById(`email`).value
    const password= document.getElementById(`password`).value
    const cpw= document.getElementById(`cpw`).value

    //condition password should match cpw
    if(password === cpw){
        //send request to server fetch(`URL`,options)
        fetch(`http://localhost:3000/users/add-user`,{
            method:`POST`,
            headers: {
                "Content-Type":"application/json"
            },
            body: JSON.stringify({
                fullName: fname,
                email: email,
                password: password
            })
        }).then(result => result.json().then(result =>{
            alert(result.message)
            window.location.replace("./login.html")}
            ))
        
    }
})
