//get item from local storage

const token = localStorage.getItem("token")
const admin = localStorage.getItem("admin")
// console.log(admin)
const orderContainer = document.getElementById("anyContainer")
const adminButton = document.getElementById("adminButton")
let cardFooter;
let orders;

if(admin == `false`){
    //regular user
    //send request to get all orders of a user
   
    fetch(`http://localhost:3000/orders/user-orders`, {
        method:"GET",
        headers:{
            "Authorization": `Bearer ${token}`
        }
    })
    //wait for servers response
    .then(result => result.json())
    .then(result => {
        // console.log(result)
    //display the products using card if result array is not empty use length for if condition
        if(result.length < 1){
            return alert(`No Products available`)
        }else{
            
                //get product details
                // async() => {
                //     const res = await fetch(`http://localhost:3000/products/${productId}`,{
                //     method:"GET",
                //     headers:{
                //         "Authorization": `Bearer ${token}`
                //     }})
                //     const data = await res.json()
                //     return await data.productName
                // }
            orders = result.map(order =>{
               // console.log(product)
                //destructure
                const {productId, OrderDate, totalAmount,_id,quantity,productName} = order
            
                return( 
                 `   <div class="col-12 col-md-4  py-3">
                        <div class="card " style="width: 18rem;">
                        <img src=""
                         class="card-img-top" alt="">
                            <div class="card-body">
                              
                              <h4 class="card-text text-center">${productName}</h4>
                              <h5 class="card-title">Ordered on: ${OrderDate}</h5>
                              <p class="card-text text-left">Product Id: ${productId}</p>
                              <p class="card-text text-left">Order Id: ${_id}</p>
                              <p class="card-text text-right">Total: ${totalAmount} php</p>
                              <p class="card-text text-right">Quantity: ${quantity} pcs</p>
                        
                            </div>
                          </div>
                    </div>
                  `)
            }).join(" ") 
            // console.log(orders)
               //     get product details
           
            orderContainer.innerHTML = orders
        }
    })

    

} else {
    //admin
    // console.log(`admin`)
     //admin user
    //send request to get all products
    fetch(`http://localhost:3000/orders/`, {
        method:"GET",
        headers:{
            "Authorization": `Bearer ${token}`
        }
    })
    //wait for servers response
    .then(result => result.json())
    .then(result => {
        // console.log(result)
    //display the products using card if result array is not empty use length for if condition
        if(result.length < 1){
            let adminButtons =
            `<a class="btn btn-info m-3" href="./createProduct.html">Add Product</a>
            <a class="btn btn-info m-3" href="./dashboard.html">Dashboard</a>
            <a class="btn btn-info m-3" href="./orders.html">Orders</a>`
            adminButton.innerHTML = adminButtons
            return `No Orders available`
        }else{
            //return all products by making a copy of the objects in the array
            
            orders = result.map(order =>{
               console.log(order)
                //destructure
                const {productId, OrderDate, totalAmount,_id,quantity,productName} = order
            
                   
                 
                return( 
                 `   <div class="col-12 col-md-4">
                        <div class="card my-4 text-center" style="width: 18rem;">
                            <img src="http://via.placeholder.com/640x360
                            " class="card-img-top" alt="">
                            <div class="card-body">
                              <h5 class="card-title">${OrderDate}</h5>
                              <h4 class="card-text text-left">${productName}</h4>
                              <p class="card-text text-right">Quantity: ${quantity} pieces</p>
                              <p class="card-text text-right">Total: ${totalAmount} php</p>
                              <div class="card-footer">
                                    ${cardFooter}
                              </div>
                            </div>
                          </div>
                    </div>
                    `
                )
            }).join(" ") 
            // console.log(products)
            orderContainer.innerHTML = orders

            let adminButtons = `<a class="btn btn-info m-3" href="./createProduct.html">Add Product</a>
                                 <a class="btn btn-info m-3" href="./dashboard.html">Dashboard</a>
                                 <a class="btn btn-info m-3" href="./orders.html">Orders</a>`
            adminButton.innerHTML = adminButtons

            // const archive = document.getElementById("confirmation")
            // archive.addEventListener(`click`,(e)=>{
            //     e.preventDefault();
            //     //delete product
            //     fetch(`http://localhost:3000/orders/delete-order/${_id}`,{
            //             method: 'DELETE',
            //             headers: {
            //             },
            //             body: JSON.stringify({
            //             })
            //         })
            //         .then(result=>{
            //             let resp = result.json();
            //             resp.then(result=>console.log(result))
            //             alert(`Product ${productName} was updated`)
            //             window.location.replace("./products.html")
            //         })
            
            // })
        }
    })
}